var webpack = require('webpack');
module.exports = {
	devtool:"inline-source-map",
	entry:[
		'webpack-hot-middleware/client',
		'./client/index.js'
	],
	output:{
		path: require("path").resolve("./dist"),
		filename:"bundle.js",
		publickPath:"/"
	},
	plugins:[
		new webpack.optimize.OccurrenceOrderPlugin(),
		new webpack.HotModuleReplacementPlugin(),
		new webpack.NoErrorsPlugin()
	],
	module:{
		loaders:[
			{
				test:/\.js$/,
				loader: 'babel-loader',
				exclude:/node_modules/,
				query:{
					presets:['es2015','react','react-hmre']
				}
			},
			 {
		        test: /\.css?$/,
		        loaders: [ 'style', 'raw' ],
		        exclude:/node_modules/,
		        include: __dirname
		      }
		]
	}
}
import React from 'react';
import NewUserInput from './NewUserInput'
import UsersList from './UsersList'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import actions from "../redux/actions"
import FilterInput from "./FilterInput"
import './app.css'

class App extends React.Component{
	render(){
		return (
				<div>
					<h3>Users list</h3>
					<NewUserInput addUser={this.props.actions.addUser}/>
					<FilterInput filtUser={this.props.actions.filtUser}/>
					<UsersList 
						deleteUser={this.props.actions.deleteUser}
						userlist={this.props.userlist} />
				</div>
			)
	}
}

function mapStateToProps (state) {
	return state;
}

function mapDispatchToProps (dispatch){
	return {
		actions: bindActionCreators(actions,dispatch)
	}

}

export default connect (mapStateToProps, mapDispatchToProps)(App);
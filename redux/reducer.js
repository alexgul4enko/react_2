const constance = {
	ADD_USER : "ADD_USER",
	DELL_USER : "DELL_USER",
	CHECK_USER : "CHECK_USER"
};
function getId (state){
	return state.userlist.reduce((maxId,user) => {
		return Math.max(user.id,maxId);
	}, -1)+1
}

export default function reducer(state, action){
	switch (action.type){
		case constance.ADD_USER:
			return Object.assign({},state,{
				userlist:[{
					text: action.text,
					matchfilter:true,
					id:getId(state)
				},...state.userlist]
			})
		case constance.CHECK_USER:
			return Object.assign({},state,{
				userlist:state.userlist.map((user)=>{

					return Object.assign({},user,
						{matchfilter: action.value?
								user.text.toUpperCase().indexOf(action.value.toUpperCase())>=0
								:true}) 
						
						

				})
			})

		case constance.DELL_USER:
			return Object.assign({},state,{
				userlist:state.userlist.filter((user)=>{
					return user.id !== action.id;
				})
			})
		default:
		return state;
	}
}
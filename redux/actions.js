const constance = {
	ADD_USER : "ADD_USER",
	DELL_USER : "DELL_USER",
	CHECK_USER : "CHECK_USER"
};
let actions = {
	addUser: function (text){

		return{
			type:constance.ADD_USER,
			text:text
		}
	},
	filtUser: function(value){
		console.log(value);
		return{
			type:constance.CHECK_USER,
			value:value
		}
	},
	deleteUser:function(id){
		return{
			type:constance.DELL_USER,
			id:id
		}

	}
}

export default actions;
